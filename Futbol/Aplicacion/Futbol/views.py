from django.shortcuts import render, redirect
from .models import *
from django.contrib import messages
from django.db.models.deletion import ProtectedError
from django.http import HttpResponse



def listadoEquipo(request):
    equipoBdd = Equipo.objects.all()
    return render(request, 'listadoEquipo.html', {'equipos': equipoBdd})


def guardarEquipo(request):
    nombre_equi = request.POST["nombre_equi"]
    siglas_equi = request.POST["siglas_equi"]
    fundacion_equi = request.POST["fundacion_equi"]
    region_equi = request.POST["region_equi"]
    numero_titulos_equi = request.POST["numero_titulos_equi"]
    nuevoEquipo = Equipo.objects.create(nombre_equi=nombre_equi, siglas_equi=siglas_equi, fundacion_equi=fundacion_equi, region_equi=region_equi, numero_titulos_equi=numero_titulos_equi)
    messages.success(request, '¡Equipo guardado exitosamente!')
    return redirect('listadoEquipo')

def eliminarEquipo(request, id_equi):
    try:
        equipo = Equipo.objects.get(pk=id_equi)
        equipo.delete()
        messages.success(request, 'Equipo eliminado correctamente.')
    except Equipo.DoesNotExist:
        messages.error(request, 'La Equipo que intentas eliminar no existe.')
    except ProtectedError:
        messages.error(request, 'No se puede eliminar la Mesa por motivos de Datos.')
    return redirect('listadoEquipo')

def editarEquipo(request, id_equi):
    equipoEditar = Equipo.objects.get(id_equi=id_equi)
    return render(request, 'editarEquipo.html', {'equipos': equipoEditar})


def procesarActualizacioEquipos(request):
    id_equi = request.POST["id_equi"]
    nombre_equi = request.POST["nombre_equi"]
    siglas_equi = request.POST["siglas_equi"]
    fundacion_equi = request.POST["fundacion_equi"]
    region_equi = request.POST["region_equi"]
    numero_titulos_equi = request.POST["numero_titulos_equi"]
    # Insertando datos mediante el ORM de DJANGO
    equipoEditar = Equipo.objects.get(id_equi=id_equi)
    equipoEditar.nombre_equi = nombre_equi
    equipoEditar.siglas_equi = siglas_equi
    equipoEditar.fundacion_equi = fundacion_equi
    equipoEditar.region_equi = region_equi
    equipoEditar.numero_titulos_equi = numero_titulos_equi
    equipoEditar.save()
    messages.success(request, 'Equipo Actualizado Exitosamente')
    return redirect('listadoEquipo')
###########################################################################################################################################
def listadoPosicion(request):
    posicionBdd = Posicion.objects.all()
    return render(request, 'listadoPosicion.html', {'posiciones': posicionBdd})


def guardarPosicion(request):
    nombre_pos = request.POST["nombre_pos"]
    descripcion_pos = request.POST["descripcion_pos"]
    nuevoPosicion = Posicion.objects.create(nombre_pos=nombre_pos, descripcion_pos=descripcion_pos)
    messages.success(request, '¡Posicion guardado exitosamente!')
    return redirect('listadoPosicion')

def eliminarPosicion(request, id_pos):
    try:
        posicion = Posicion.objects.get(pk=id_pos)
        posicion.delete()
        messages.success(request, 'Posicion eliminado correctamente.')
    except Posicion.DoesNotExist:
        messages.error(request, 'La Posicion que intentas eliminar no existe.')
    except ProtectedError:
        messages.error(request, 'No se puede eliminar la Mesa por motivos de Datos.')
    return redirect('listadoPosicion')

def editarPosicion(request, id_pos):
    posicionEditar = Posicion.objects.get(id_pos=id_pos)
    return render(request, 'editarPosicion.html', {'posiciones': posicionEditar})


def procesarActualizacioPosicion(request):
    id_pos = request.POST["id_pos"]
    nombre_pos = request.POST["nombre_pos"]
    descripcion_pos = request.POST["descripcion_pos"]
    # Insertando datos mediante el ORM de DJANGO
    posicionEditar = Posicion.objects.get(id_pos=id_pos)
    posicionEditar.nombre_pos = nombre_pos
    posicionEditar.descripcion_pos = descripcion_pos
    posicionEditar.save()
    messages.success(request, 'Posicion Actualizado Exitosamente')
    return redirect('listadoPosicion')
##############################################################################################################

def listadoJugador(request):
    jugadorBdd = Jugador.objects.all()
    equipoBdd = Equipo.objects.all()
    posicionBdd = Posicion.objects.all()
    return render(request, 'listadoJugador.html', {'jugadores': jugadorBdd,'equipos':equipoBdd,'posiciones':posicionBdd })


def guardarJugadores(request):
    id_equi_fk_equi=request.POST["id_equi_fk_equi"]
    fk_equiSeleccionado=Equipo.objects.get(id_equi=id_equi_fk_equi)
    id_pos_fk_pos=request.POST["id_pos_posicion"]
    fk_posSeleccionado=Posicion.objects.get(id_pos=id_pos_fk_pos)
    apellido_jug=request.POST["apellido_jug"]
    nombre_jug = request.POST.get('nombre_jug')
    estatura_jug=request.POST["estatura_jug"]
    salario_jug=request.POST["salario_jug"]
    estado_jug=request.POST["estado_jug"]

    nuevoJugador = Jugador.objects.create(
        apellido_jug=apellido_jug,
        nombre_jug=nombre_jug,
        estatura_jug=estatura_jug,
        salario_jug=salario_jug,
        estado_jug=estado_jug,
        fk_pos=fk_posSeleccionado,
        fk_equi=fk_equiSeleccionado
    )

    messages.success(request, 'Jugadores guardada exitosamente')
    return redirect('listadoJugador')


def eliminarJugadores(request, id_jug):
    try:
        jugador = Jugador.objects.get(pk=id_jug)
        jugador.delete()
        messages.success(request, 'Jugador eliminado correctamente.')
    except Jugador.DoesNotExist:
        messages.error(request, 'La Jugador que intentas eliminar no existe.')
    except ProtectedError:
        messages.error(request, 'No se puede eliminar el Jugador porque hay productos relacionados.')
    return redirect('listadoJugador')

def editarJugador(request, id_jug):
    jugadorEditar=Jugador.objects.get(id_jug=id_jug)
    equipoBdd = Equipo.objects.all()
    posicionBdd = Posicion.objects.all()
    return render(request, 'editarJugador.html', {'jugadores': jugadorEditar,'equipos':equipoBdd,'posiciones':posicionBdd })

def procesarActualizacionJugador(request):
    id_jug=request.POST["id_jug"]
    id_equi_fk_equi=request.POST["id_equi_fk_equi"]
    fk_equiSeleccionado=Equipo.objects.get(id_equi=id_equi_fk_equi)
    id_pos_fk_pos=request.POST["id_pos_posicion"]
    fk_posSeleccionado=Posicion.objects.get(id_pos=id_pos_fk_pos)
    apellido_jug=request.POST["apellido_jug"]
    nombre_jug = request.POST.get('nombre_jug')
    estatura_jug=request.POST["estatura_jug"]
    salario_jug=request.POST["salario_jug"]
    estado_jug=request.POST["estado_jug"]

    #Insertando datos mediante el ORM de DJANGO
    jugadorEditar=Jugador.objects.get(id_jug=id_jug)
    jugadorEditar.fk_pos=fk_posSeleccionado
    jugadorEditar.fk_equi=fk_equiSeleccionado
    jugadorEditar.apellido_jug=apellido_jug
    jugadorEditar.nombre_jug=nombre_jug
    jugadorEditar.estatura_jug=estatura_jug
    jugadorEditar.salario_jug=salario_jug
    jugadorEditar.estado_jug=estado_jug
    jugadorEditar.save()
    messages.success(request,
      'Reserva ACTUALIZADO Exitosamente')
    return redirect('listadoJugador')
