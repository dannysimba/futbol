# Generated by Django 3.2.25 on 2024-06-27 12:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Futbol', '0009_auto_20240627_0702'),
    ]

    operations = [
        migrations.RenameField(
            model_name='jugador',
            old_name='fk_id_equi',
            new_name='fk_equi',
        ),
        migrations.RenameField(
            model_name='jugador',
            old_name='fk_id_pos',
            new_name='fk_pos',
        ),
    ]
