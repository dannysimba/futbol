from django.db import models

class Equipo(models.Model):
    id_equi = models.AutoField(primary_key=True)
    nombre_equi = models.CharField(max_length=50)
    siglas_equi = models.CharField(max_length=10)
    fundacion_equi = models.CharField(max_length=10)
    region_equi = models.CharField(max_length=10, null=True, blank=True)
    numero_titulos_equi = models.IntegerField()

    def __str__(self):
        return f"{self.id_equi}: {self.nombre_equi}"

class Posicion(models.Model):
    id_pos = models.AutoField(primary_key=True)
    nombre_pos = models.CharField(max_length=50)
    descripcion_pos = models.CharField(max_length=500)

    def __str__(self):
        return f"{self.id_pos}: {self.nombre_pos}"

class Jugador(models.Model):
    id_jug = models.AutoField(primary_key=True)
    apellido_jug = models.CharField(max_length=50)
    nombre_jug = models.CharField(max_length=10)
    estatura_jug = models.DecimalField(max_digits=4, decimal_places=2)
    salario_jug = models.CharField(max_length=10)
    estado_jug = models.CharField(max_length=10)
    fk_pos = models.ForeignKey(Posicion, null=True, blank=True, on_delete=models.PROTECT)
    fk_equi = models.ForeignKey(Equipo, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        return f"{self.id_jug}: {self.apellido_jug}"
