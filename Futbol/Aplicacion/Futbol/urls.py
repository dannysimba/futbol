from django.urls import path
from . import views

urlpatterns = [

    path('',views.listadoEquipo, name='listadoEquipo'),
    path('guardarEquipo/', views.guardarEquipo, name='guardarEquipo'),
    path('eliminarEquipo/<int:id_equi>/', views.eliminarEquipo, name='eliminarEquipo'),
    path('editarEquipo/<int:id_equi>/', views.editarEquipo, name='editarEquipo'),
    path('procesarActualizacioEquipos/', views.procesarActualizacioEquipos, name='procesarActualizacioEquipos'),

    path('listadoPosicion/',views.listadoPosicion, name='listadoPosicion'),
    path('guardarPosicion/', views.guardarPosicion, name='guardarPosicion'),
    path('eliminarPosicion/<int:id_pos>/', views.eliminarPosicion, name='eliminarPosicion'),
    path('editarPosicion/<int:id_pos>/', views.editarPosicion, name='editarPosicion'),
    path('procesarActualizacioPosicion/', views.procesarActualizacioPosicion, name='procesarActualizacioPosicion'),

    path('listadoJugador/',views.listadoJugador, name='listadoJugador'),
    path('guardarJugadores/', views.guardarJugadores, name='guardarJugadores'),
    path('eliminarJugadores/<int:id_jug>/', views.eliminarJugadores, name='eliminarJugadores'),
    path('editarJugador/<int:id_jug>/', views.editarJugador, name='editarJugador'),
    path('procesarActualizacionJugador/', views.procesarActualizacionJugador, name='procesarActualizacionJugador'),


]
